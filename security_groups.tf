# LAMBDA
resource "aws_security_group" "lambda_sg" {
  name        = "demo-lambda-sg"
  description = "Security group for lambda in private subnet"
  vpc_id      = module.vpc.vpc_id

  ingress = []

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demo-lambda-sg"
  }
}

# Interface
resource "aws_security_group" "interface_sg" {
  name        = "demo-interface-sg"
  description = "Security group for interface in private subnet"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = module.vpc.private_subnets_cidr_blocks
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "demo-interface-sg"
  }
}