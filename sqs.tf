resource "aws_sqs_queue" "queue" {
  name       = "vpc-endpoint-sqs.fifo"
  fifo_queue = true
}