// zip the binary, as we can upload only zip files to AWS lambda
data "archive_file" "function_archive" {

  type        = "zip"
  source_file = local.binary_path
  output_path = local.archive_path
}


module "s3_sqs_lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "6.5.0"
  timeout = 60

  vpc_subnet_ids         = module.vpc.private_subnets
  vpc_security_group_ids = [aws_security_group.lambda_sg.id]

  function_name  = "s3-sqs-lambda"
  create_package = false
  publish        = true
  runtime        = "provided.al2023"

  handler                = "bootstrap"
  local_existing_package = data.archive_file.function_archive.output_path

  allowed_triggers = {
    APIGateway = {
      service    = "apigateway"
      source_arn = "${aws_apigatewayv2_api.apigateway.arn}/${aws_apigatewayv2_stage.stage.id}/${aws_apigatewayv2_route.route.id}"
    }
  }

  attach_cloudwatch_logs_policy     = true
  cloudwatch_logs_retention_in_days = 7
  attach_policy_statements          = true

  #Controls whether policy_statements should be added to IAM role for Lambda Function
  policy_statements = {
    ec2_permission = {
      effect = "Allow",
      actions = [
        "ec2:DescribeNetworkInterfaces",
        "ec2:CreateNetworkInterface",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeInstances",
        "ec2:AttachNetworkInterface"
      ],
      resources = ["*"]
    }
    s3_permission = {
      effect = "Allow",
      actions = [
        "s3:PutObject",
        "s3:ListBucket",
        "s3:GetObject"
      ],
      resources = [
        "${module.s3-bucket.s3_bucket_arn}",
        "${module.s3-bucket.s3_bucket_arn}/*"
      ]
    },
    sqs_permission = {
      effect = "Allow",
      actions = [
        "sqs:SendMessage",
      ],
      resources = [aws_sqs_queue.queue.arn]
    },
    lambda_permission = {
      effect = "Allow",
      actions = [
        "lambda:InvokeFunction",
        "lambda:InvokeAsync"
      ],
      resources = ["*"]
    },
    cloudwatch_permission = {
      effect = "Allow"
      actions = [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
      ]

      resources = [
        "arn:aws:logs:*:*:*",
      ]
    },
    apigateway_permission = {
      effect = "Allow",
      actions = [
        "apigateway:GET",
        "apigateway:PUT",
        "apigateway:POST",
      ],
      resources = ["*"]
    },
    apigateway_invoke_permission = {
      effect = "Allow",
      actions = [
        "execute-api:Invoke",
        "execute-api:ManageConnections"
      ],
      resources = ["arn:aws:execute-api:*:*:*"]
    },
    xray = {
      effect = "Allow",
      actions = [
        "xray:PutTraceSegments",
        "xray:PutTelemetryRecords",
        "xray:GetSamplingRules",
        "xray:GetSamplingTargets",
        "xray:GetSamplingStatisticSummaries"
      ],
      resources = ["*"]
    }
  }

  environment_variables = {
    S3_BUCKET_NAME="${module.s3-bucket.s3_bucket_id}"
    SQS_FIFO_URL="${aws_sqs_queue.queue.id}"
    SQS_MESSAGE_GROUPID="VPC-Endpoint"
    SQS_MESSAGE_BODY="Test VPC message body"
  }
}