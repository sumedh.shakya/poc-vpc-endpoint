module "s3-bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "3.15.1"

  bucket                   = "vpc-endpoint-demo-s3"
  acl                      = "private"
  control_object_ownership = true
  object_ownership         = "ObjectWriter"
}