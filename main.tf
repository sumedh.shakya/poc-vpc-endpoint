resource "aws_vpc_endpoint" "gateway_endpoint" {
  service_name    = "com.amazonaws.ap-south-1.s3"
  vpc_id          = module.vpc.vpc_id
  route_table_ids = module.vpc.private_route_table_ids
}

resource "aws_vpc_endpoint" "interface_endpoint" {
  service_name = "com.amazonaws.ap-south-1.sqs"
  vpc_id = module.vpc.vpc_id
  vpc_endpoint_type = "Interface"
  subnet_ids = module.vpc.private_subnets
  security_group_ids = [aws_security_group.interface_sg.id]
  private_dns_enabled = true  #  Whether or not to associate a private hosted zone with the specified VPC. Applicable for endpoints of type Interface.
}