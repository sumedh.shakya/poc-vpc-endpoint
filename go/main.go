package main

import (
	"context"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/sqs"

	"github.com/aws/aws-xray-sdk-go/instrumentation/awsv2"
	"github.com/aws/aws-xray-sdk-go/xray"
)

var (
	sqsClient *sqs.Client
	s3Client  *s3.Client
)

// APIGatewayProxyRequest contains data coming from the API Gateway proxy
type APIGatewayProxyRequest struct {
	Resource                        string              `json:"resource"` // The resource path defined in API Gateway
	Path                            string              `json:"path"`     // The url path for the caller
	HTTPMethod                      string              `json:"httpMethod"`
	Headers                         map[string]string   `json:"headers"`
	MultiValueHeaders               map[string][]string `json:"multiValueHeaders"`
	QueryStringParameters           map[string]string   `json:"queryStringParameters"`
	MultiValueQueryStringParameters map[string][]string `json:"multiValueQueryStringParameters"`
	PathParameters                  map[string]string   `json:"pathParameters"`
	StageVariables                  map[string]string   `json:"stageVariables"`
	Body                            string              `json:"body"`
	IsBase64Encoded                 bool                `json:"isBase64Encoded,omitempty"`
}

// APIGatewayProxyResponse configures the response to be returned by API Gateway for the request
type APIGatewayProxyResponse struct {
	StatusCode        int                 `json:"statusCode"`
	Headers           map[string]string   `json:"headers"`
	MultiValueHeaders map[string][]string `json:"multiValueHeaders"`
	Body              string              `json:"body"`
	IsBase64Encoded   bool                `json:"isBase64Encoded,omitempty"`
}

func GetEnv(key string) string {
	return os.Getenv(key)
}

func SendMessageToSQS(ctx context.Context) error {
	log.Println("Sending message to SQS queue")
	_, err := sqsClient.SendMessage(ctx, &sqs.SendMessageInput{
		MessageGroupId:         aws.String(GetEnv("SQS_MESSAGE_GROUPID")),
		MessageBody:            aws.String(GetEnv("SQS_MESSAGE_BODY")),
		MessageDeduplicationId: aws.String(strconv.FormatInt(time.Now().Unix(), 10)),
		QueueUrl:               aws.String(GetEnv("SQS_FIFO_URL")),
	})
	return err
}

func UploadToS3(ctx context.Context) error {
	log.Println("Uploading file to S3 bucket")

	reader := strings.NewReader("this is test for vpc endpoint with file upload to s3 and sending message to sqs.")

	_, err := s3Client.PutObject(ctx, &s3.PutObjectInput{
		Bucket: aws.String(GetEnv("S3_BUCKET_NAME")),
		Key:    aws.String(strconv.FormatInt(time.Now().Unix(), 10) + ".txt"),
		Body:   reader,
	})
	return err
}

func HandleS3Request(ctx context.Context) error {
	err := UploadToS3(ctx)
	if err != nil {
		log.Println("Error uploading data to S3:", err)
		return err
	}
	log.Println("File Uploaded!")
	return nil
}

func HandleSQSRequest(ctx context.Context) error {
	err := SendMessageToSQS(ctx)
	if err != nil {
		log.Println("Error sending message to SQS:", err)
		return err
	}
	log.Println("Message Queued!")
	return nil
}

func HandleLambda(contx context.Context, request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ctx, root := xray.BeginSegment(contx, "VPC Endpoint")

	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		panic(err)
	}

	awsv2.AWSV2Instrumentor(&cfg.APIOptions)
	sqsClient = sqs.NewFromConfig(cfg)
	s3Client = s3.NewFromConfig(cfg)

	if strings.HasSuffix(request.Path, "s3") {
		err := HandleS3Request(ctx)
		root.Close(nil)
		if err != nil {
			return events.APIGatewayProxyResponse{Body: "S3 failed --> " + err.Error(), StatusCode: 400}, err
		}
		return events.APIGatewayProxyResponse{Body: "S3 Success", StatusCode: 200}, nil

	} else if strings.HasSuffix(request.Path, "sqs") {
		err := HandleSQSRequest(ctx)
		root.Close(nil)
		if err != nil {
			return events.APIGatewayProxyResponse{Body: "SQS failed --> " + err.Error(), StatusCode: 400}, err
		}
		return events.APIGatewayProxyResponse{Body: "SQS Success", StatusCode: 200}, nil
	}

	root.Close(nil)
	return events.APIGatewayProxyResponse{Body: "Lambda Call Success", StatusCode: 200}, nil
}

func main() {
	lambda.Start(HandleLambda)
}
