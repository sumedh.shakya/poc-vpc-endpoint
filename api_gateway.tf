// HTTP API GATEWAY
resource "aws_apigatewayv2_api" "apigateway" {
  name          = "vpc-endpoint-apigateway"
  protocol_type = "HTTP"

  cors_configuration {
    allow_headers = ["*"]
    allow_methods = [
      "GET",
    ]
    allow_origins = [
      "*" // NOTE: here we should provide a particular domain, but for the sake of simplicity we will use "*"
    ]
    expose_headers = []
    max_age        = 0
  }
}

# DEPLOYMENT
resource "aws_apigatewayv2_deployment" "deployment" {
  api_id = aws_apigatewayv2_api.apigateway.id

  depends_on = [ aws_apigatewayv2_route.route ]
}

# STAGE
resource "aws_apigatewayv2_stage" "stage" {
  api_id = aws_apigatewayv2_api.apigateway.id
  deployment_id = aws_apigatewayv2_deployment.deployment.id

  name        = "vpc-endpoint-apigateway-stage"

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.apigateway_loggroup.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
  depends_on = [aws_cloudwatch_log_group.apigateway_loggroup]
}

resource "aws_cloudwatch_log_group" "apigateway_loggroup" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.apigateway.name}"

  retention_in_days = 7
}

# INTEGRATION
resource "aws_apigatewayv2_integration" "integration" {
  api_id = aws_apigatewayv2_api.apigateway.id
  integration_uri  = module.s3_sqs_lambda.lambda_function_invoke_arn
  integration_type = "AWS_PROXY"
}


# ROUTE
resource "aws_apigatewayv2_route" "route" {
  api_id    = aws_apigatewayv2_api.apigateway.id
  route_key = "GET /api/{proxy+}"
  target    = "integrations/${aws_apigatewayv2_integration.integration.id}"
}

# resource "aws_apigatewayv2_route" "apigateway_lambda_route_sqs" {
#   api_id    = aws_apigatewayv2_api.apigateway.id
#   route_key = "GET /api/sqs"
#   target    = "integrations/${aws_apigatewayv2_integration.apigateway_lambda_integration.id}"
# }

# PERMISSION
resource "aws_lambda_permission" "permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.s3_sqs_lambda.lambda_function_name

  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.apigateway.execution_arn}/*/*"
}

# resource "aws_lambda_permission" "apigateway_lambda_permission_s3" {
#   statement_id  = "AllowExecutionFromAPIGatewayS3"
#   action        = "lambda:InvokeFunction"
#   function_name = module.s3_sqs_lambda.lambda_function_name
#   principal = "apigateway.amazonaws.com"
#   source_arn = "${aws_apigatewayv2_api.apigateway.execution_arn}/${aws_apigatewayv2_stage.apigateway_stage.name}/*/api/s3"
#   # source_arn = "${aws_apigatewayv2_api.apigateway.execution_arn}/*/${aws_apigatewayv2_route.apigateway_lambda_route_s3.route_key}"
# }

# resource "aws_lambda_permission" "apigateway_lambda_permission_sqs" {
#   statement_id  = "AllowExecutionFromAPIGatewaySQS"
#   action        = "lambda:InvokeFunction"
#   function_name = module.s3_sqs_lambda.lambda_function_name
#   principal = "apigateway.amazonaws.com"
#   source_arn = "${aws_apigatewayv2_api.apigateway.execution_arn}/${aws_apigatewayv2_stage.apigateway_stage.name}/*/api/sqs"
# }