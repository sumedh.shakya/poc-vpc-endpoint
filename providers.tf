provider "aws" {
  region = "ap-south-1"

  #   assume_role {
  #     role_arn = var.AWS_ROLE_ARN
  #   }

  default_tags {
    tags = {
      Management = "terraform"
      Author     = "Sumedh"
    }
  }
}