module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.4.0"

  name = var.vpc_name
  vpc_tags = {
    Name = "${var.vpc_name}"
  }

  cidr            = local.cidr
  azs             = local.azs
  public_subnets  = [for k, v in local.azs : cidrsubnet(local.cidr, 8, k)]
  private_subnets = [for k, v in local.azs : cidrsubnet(local.cidr, 8, k + 10)]

  create_igw = true
  igw_tags = {
    Name = "${var.vpc_name}-IG"
  }

  enable_nat_gateway = true
  nat_gateway_tags = {
    Name = "${var.vpc_name}-NG"
  }

  enable_dns_hostnames = true
}

resource "aws_flow_log" "vpc_flow_log" {
  iam_role_arn             = aws_iam_role.vpc_flow_log.arn
  log_destination_type     = "cloud-watch-logs"
  log_destination          = aws_cloudwatch_log_group.cw_flow_log.arn
  traffic_type             = "ALL"
  vpc_id                   = module.vpc.vpc_id
  max_aggregation_interval = 60 # must be 60 is transit gateway id or transit gateway attachment id is specified
}

resource "aws_cloudwatch_log_group" "cw_flow_log" {
  name = "/aws/vpc/vpc-flow-log"
}