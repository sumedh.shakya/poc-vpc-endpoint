terraform {
  backend "remote" {
    organization = "sumedh-shakya"
    hostname     = "app.terraform.io"
    workspaces {
      prefix = "poc-"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    archive = {
      source = "hashicorp/archive"
    }
  }
}