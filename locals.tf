locals {
  cidr = "10.12.0.0/16"
  azs  = ["ap-south-1a"]

  role_policy_arns = [
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  ]

  function_name = "bootstrap"
  binary_path   = "${path.module}/go/${local.function_name}"
  archive_path  = "${path.module}/go/${local.function_name}.zip"
}

