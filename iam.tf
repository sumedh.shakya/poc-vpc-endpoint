# VPC FLOW LOG
resource "aws_iam_role" "vpc_flow_log" {
  name = "vpc-flow-log-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "vpc-flow-logs.amazonaws.com"
      }
    }]
  })
  inline_policy {
    name   = "vpc-flow-log-role-policy"
    policy = data.aws_iam_policy_document.flow_log_inline_policy.json
  }
}